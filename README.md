JPEG-2000 GDK-Pixbuf Loader library
===================================

This is a JPEG2000 loader based on [OpenJPEG](https://www.openjpeg.org/).

It's experimental, and not heavily tested, and it provides the same kind of
support that the old libjasper-based loader in GdkPixbuf used to.

A better tested JPEG2000 loader is available at:

https://notabug.org/necklace/jp2-pixbuf-loader

Building from source
--------------------

 - Build:

```
meson builddir
ninja -C builddir
```
  
 - Install:

```
sudo ninja -C builddir install
```
