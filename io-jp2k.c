/* GdkPixbuf library - JPEG2000 Image Loader
 *
 * SPDX-License-Identifier: LGPL-2.0-or-later
 *
 * Copyright 2020  GNOME Foundation
 */

#include <openjpeg.h>
#include <string.h>

#define GDK_PIXBUF_ENABLE_BACKEND
#include <gdk-pixbuf/gdk-pixbuf.h>
#undef  GDK_PIXBUF_ENABLE_BACKEND

/* Progressive loader context */
struct jp2k_context
{
        GdkPixbufModuleSizeFunc size_func;
        GdkPixbufModuleUpdatedFunc update_func;
        GdkPixbufModulePreparedFunc prepare_func;
        gpointer user_data;

        opj_dparameters_t params;
        opj_stream_t *stream;
        opj_codec_t *decoder;

        int width;
        int height;

        // The result pixbuf
        GdkPixbuf *pixbuf;

        // The bytes buffer we use to incrementally store the data we read
        GByteArray *buffer;

        // The immutable bytes buffer we feed to openjpeg2
        GBytes *bytes;
};

static void
jp2k_context_free (struct jp2k_context *context)
{
        g_clear_object (&context->pixbuf);
        g_clear_pointer (&context->buffer, g_byte_array_unref);
        g_clear_pointer (&context->bytes, g_bytes_unref);

        g_clear_pointer (&context->stream, opj_stream_destroy);
        g_clear_pointer (&context->decoder, opj_destroy_codec);

        g_free (context);
}

static gpointer
gdk_pixbuf__jp2k_image_begin_load (GdkPixbufModuleSizeFunc size_func,
                                   GdkPixbufModulePreparedFunc prepare_func,
                                   GdkPixbufModuleUpdatedFunc update_func,
                                   gpointer user_data,
                                   GError **error G_GNUC_UNUSED)
{
        struct jp2k_context *context = g_new0 (struct jp2k_context, 1);

        context->size_func = size_func;
        context->prepare_func = prepare_func;
        context->update_func  = update_func;
        context->user_data = user_data;

        context->buffer = g_byte_array_sized_new (8192);

        return context;
}

static const char *
color_space_to_str (OPJ_COLOR_SPACE cspc)
{
        switch (cspc) {
                case OPJ_CLRSPC_UNKNOWN:
                        return "unknown";
                case OPJ_CLRSPC_UNSPECIFIED:
                        return "unspecified";
                case OPJ_CLRSPC_SRGB:
                        return "sRGB";
                case OPJ_CLRSPC_GRAY:
                        return "grayscale";
                case OPJ_CLRSPC_SYCC:
                        return "YUV";
                case OPJ_CLRSPC_EYCC:
                        return "e-YCC";
                case OPJ_CLRSPC_CMYK:
                        return "CMYK";
                default:
                        g_assert_not_reached ();
        }

        return "INVALID";
}

static inline int
sample_data (opj_image_t *image,
             int x,
             int y,
             int width,
             int height,
             OPJ_UINT32 channel,
             gboolean subsampling)
{
        int offset = 0;
        int res = 0;

        if (subsampling) {
                int sub_x = width / image->comps[channel].w;
                int sub_y = height / image->comps[channel].h;

                offset = (y / sub_y) * image->comps[channel].w + (x / sub_x);
        }
        else {
                offset = y * width + x;
        }

        res = image->comps[channel].data[offset];
        res += (image->comps[channel].sgnd ? 1 << (image->comps[channel].prec - 1) : 0);

        return res;
}

static gboolean
jp2k_try_load (struct jp2k_context *context,
               GError **error)
{
        opj_image_t *image = NULL;

        if (!opj_read_header (context->stream, context->decoder, &image)) {
                g_set_error_literal (error, GDK_PIXBUF_ERROR,
                                     GDK_PIXBUF_ERROR_UNKNOWN_TYPE,
                                     "Unknown JPEG2000 image type");
                opj_image_destroy (image);
                return FALSE;
        }

        context->width = image->x1 - image->x0;
        context->height = image->y1 - image->y0;

        g_debug ("image size: %d x %d", context->width, context->height);

        {
                int width = context->width;
                int height = context->height;

                context->size_func (&width, &height, context->user_data);

                if (width == 0 || height == 0) {
                        g_set_error_literal (error, GDK_PIXBUF_ERROR,
                                             GDK_PIXBUF_ERROR_CORRUPT_IMAGE,
                                             "Transformed JPEG2000 has zero width or height");
                        opj_image_destroy (image);
                        return FALSE;
                }
        }

        if (!opj_decode (context->decoder, context->stream, image)) {
                g_set_error_literal (error, GDK_PIXBUF_ERROR,
                                     GDK_PIXBUF_ERROR_CORRUPT_IMAGE,
                                     "Unable to decode the JPEG2000 image");
                opj_image_destroy (image);
                return FALSE;
        }

        g_debug ("image color space: %s (%d)", color_space_to_str (image->color_space), image->color_space);

        if (image->color_space == OPJ_CLRSPC_UNSPECIFIED) {
                if (image->numcomps == 3 &&
                    image->comps[0].dx == image->comps[0].dy &&
                    image->comps[1].dx != 1) {
                        image->color_space = OPJ_CLRSPC_SYCC;
                }
        }
        else if (image->numcomps <= 2) {
                image->color_space = OPJ_CLRSPC_GRAY;
        }

        if (image->color_space != OPJ_CLRSPC_SRGB && image->color_space != OPJ_CLRSPC_GRAY) {
                g_set_error_literal (error, GDK_PIXBUF_ERROR,
                                     GDK_PIXBUF_ERROR_UNKNOWN_TYPE,
                                     "Unsupported JPEG2000 color space");
                opj_image_destroy (image);
                return FALSE;
        }

        if (image->color_space == OPJ_CLRSPC_SRGB && (image->numcomps > 4 || image->numcomps < 3)) {
                g_set_error_literal (error, GDK_PIXBUF_ERROR,
                                     GDK_PIXBUF_ERROR_UNKNOWN_TYPE,
                                     "Unsupported number of JPEG2000 components");
                opj_image_destroy (image);
                return FALSE;
        }

        int bits_per_pixel = 8; // hardcoded
        gboolean has_alpha = (image->numcomps == 4 || image->numcomps == 2);

        gboolean subsampling = image->comps[0].dx != 1 ||
                               image->comps[0].dy != 1 ||
                               image->comps[1].dx != 1 ||
                               image->comps[1].dy != 1 ||
                               image->comps[2].dx != 1 ||
                               image->comps[2].dy != 1;

        int adjustment[4] = { 0, 0, 0, 0 };
        for (OPJ_UINT32 component = 0; component < image->numcomps; component += 1) {
                if (!image->comps[0].data) {
                        g_set_error_literal (error, GDK_PIXBUF_ERROR,
                                             GDK_PIXBUF_ERROR_CORRUPT_IMAGE,
                                             "Invalid JPEG2000 image data");
                        return FALSE;
                }

                if (image->comps[component].prec > 8) {
                        adjustment[component] = image->comps[component].prec - 8;
                }
        }

        context->pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
                                          has_alpha, bits_per_pixel,
                                          context->width,
                                          context->height);

        context->prepare_func (context->pixbuf, NULL, context->user_data);

        guchar *pixels = gdk_pixbuf_get_pixels (context->pixbuf);
        guint rowstride = gdk_pixbuf_get_rowstride (context->pixbuf);

        for (int y = 0; y < context->height; y++) {
                int x;

                for (x = 0; x < context->width; x++) {
                        int channel[4] = { 0, 0, 0, 0 };

                        for (OPJ_UINT32 component = 0; component < image->numcomps; component += 1) {
                                channel[component] = sample_data (image,
                                                                  x, y,
                                                                  context->width, context->height,
                                                                  component,
                                                                  subsampling);
                        }

                        int r, g, b, a;

                        if (image->color_space == OPJ_CLRSPC_GRAY) {
                                r = g = b = channel[0];
                                a = has_alpha ? channel[1] : 0xFF;
                        }
                        else if (image->color_space == OPJ_CLRSPC_SRGB) {
                                r = channel[0];
                                g = channel[1];
                                b = channel[2];
                                a = has_alpha ? channel[3] : 0xFF;
                        }
                        else {
                                r = g = b = a = 0;
                        }

                        int adjusted_r = (r >> adjustment[0]) + ((r >> (adjustment[0] - 1)) % 2);
                        int adjusted_g = (g >> adjustment[1]) + ((g >> (adjustment[1] - 1)) % 2);
                        int adjusted_b = (b >> adjustment[2]) + ((b >> (adjustment[2] - 1)) % 2);
                        int adjusted_a = has_alpha
                                       ? (a >> adjustment[3]) + ((a >> (adjustment[3] - 1)) % 2)
                                       : 0xFF;

                        pixels[y * rowstride + x + 0] = adjusted_r;
                        pixels[y * rowstride + x + 1] = adjusted_g;
                        pixels[y * rowstride + x + 2] = adjusted_b;
                        if (has_alpha) {
                                pixels[y * rowstride + x + 3] = adjusted_a;
                        }
                }

                // Update once per row
                context->update_func (context->pixbuf, 0, y, x, 1, context->user_data);
        }

        return TRUE;
}

static void
jp2k_setup_parameters (opj_dparameters_t *params)
{
        memset (params, 0, sizeof (opj_dparameters_t));
        opj_set_default_decoder_parameters (params);
}

struct jp2k_reader
{
        GBytes *data;
        gsize offset;
};

static OPJ_SIZE_T
jp2k_stream_read (void *buffer,
                  OPJ_SIZE_T bytes,
                  void *user_data)
{
        struct jp2k_reader *reader = user_data;
        gsize data_len = g_bytes_get_size (reader->data);

        if (reader->offset == data_len) {
                return -1;
        }

        OPJ_SIZE_T len = reader->offset + bytes > data_len
                       ? data_len - reader->offset
                       : bytes;

        memcpy (buffer, g_bytes_get_data (reader->data, NULL), len);

        reader->offset += len;

        return len;
}

static OPJ_OFF_T
jp2k_stream_skip (OPJ_OFF_T bytes,
                  void *user_data)
{
        struct jp2k_reader *reader = user_data;
        gsize data_len = g_bytes_get_size (reader->data);

        OPJ_OFF_T skip = reader->offset + bytes > data_len
                       ? (OPJ_OFF_T) (data_len - reader->offset)
                       : bytes;

        reader->offset += skip;

        return skip;
}

static OPJ_BOOL
jp2k_stream_seek (OPJ_OFF_T bytes,
                  void *user_data)
{
        struct jp2k_reader *reader = user_data;

        if ((gsize) bytes > g_bytes_get_size (reader->data)) {
                return OPJ_FALSE;
        }

        reader->offset = (gsize) bytes;

        return OPJ_TRUE;
}

static gboolean
gdk_pixbuf__jp2k_image_stop_load (gpointer data,
                                  GError **error)
{
        struct jp2k_context *context = data;
        gboolean res = FALSE;

        context->bytes = g_byte_array_free_to_bytes (context->buffer);
        context->buffer = NULL;

        context->decoder = opj_create_decompress (OPJ_CODEC_JP2);
        if (context->decoder == NULL) {
                g_set_error_literal (error, GDK_PIXBUF_ERROR,
                                     GDK_PIXBUF_ERROR_INSUFFICIENT_MEMORY,
                                     "Insufficient memory for decoding the image");
                goto out;
        }

        jp2k_setup_parameters (&(context->params));
        if (!opj_setup_decoder (context->decoder, &(context->params))) {
                g_set_error_literal (error, GDK_PIXBUF_ERROR,
                                     GDK_PIXBUF_ERROR_INSUFFICIENT_MEMORY,
                                     "Insufficient memory for decoding the image");
                goto out;
        }

        struct jp2k_reader reader = {
                .data = context->bytes,
                .offset = 0,
        };

        context->stream = opj_stream_default_create (OPJ_TRUE);

        opj_stream_set_user_data (context->stream, &reader, NULL);
        opj_stream_set_user_data_length (context->stream, g_bytes_get_size (reader.data));
        opj_stream_set_read_function (context->stream, jp2k_stream_read);
        opj_stream_set_skip_function (context->stream, jp2k_stream_skip);
        opj_stream_set_seek_function (context->stream, jp2k_stream_seek);

        res = jp2k_try_load (context, error);

out:
        jp2k_context_free (context);

        return res;
}

static gboolean
gdk_pixbuf__jp2k_image_load_increment (gpointer data,
                                       const guchar *buf,
                                       guint size,
                                       GError **error)
{
        struct jp2k_context *context = data;

        g_byte_array_append (context->buffer, buf, size);

        *error = NULL;

        return TRUE;
}

G_MODULE_EXPORT void
fill_vtable (GdkPixbufModule *module);

G_MODULE_EXPORT void
fill_vtable (GdkPixbufModule *module)
{
        module->begin_load = gdk_pixbuf__jp2k_image_begin_load;
        module->stop_load = gdk_pixbuf__jp2k_image_stop_load;
        module->load_increment = gdk_pixbuf__jp2k_image_load_increment;
}

G_MODULE_EXPORT void
fill_info (GdkPixbufFormat *info);

G_MODULE_EXPORT void
fill_info (GdkPixbufFormat *info)
{
        static GdkPixbufModulePattern signature[] = {
                { "    jP",               "!!!!  ", 100 },
                { "\xff\x4f\xff\x51\x00", NULL,     100 },
                { NULL, NULL, 0 }
        };

        static char *mime_types[] = {
                "image/jp2",
                "image/jpeg2000",
                NULL
        };

        static char *extensions[] = {
                "jp2",
                NULL
        };

        info->name        = "openjp2";
        info->signature   = signature;
        info->description = "The JPEG2000 image format";
        info->mime_types  = mime_types;
        info->extensions  = extensions;
        info->flags       = GDK_PIXBUF_FORMAT_THREADSAFE;
        info->license     = "LGPL";
}
