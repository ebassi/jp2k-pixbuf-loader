#include <glib.h>
#include <gio/gio.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

static void
add_test_for_all_images (const char *prefix,
                         GFile *base,
                         GTestDataFunc test_func)
{
        GError *error = NULL;
        GFileEnumerator *enumerator =
                g_file_enumerate_children (base, "standard::name,standard::content-type", 0, NULL, &error);

        g_assert_no_error (error);

        GList *files = NULL;
        GFileInfo *info;
        while ((info = g_file_enumerator_next_file (enumerator, NULL, &error))) {
                const char *content_type = g_file_info_get_content_type (info);

                if (!g_content_type_is_a (content_type, "image/*")) {
                        continue;
                }

                GFile *next_file = g_file_get_child (base, g_file_info_get_name (info));

                files = g_list_prepend (files, g_object_ref (next_file));

                g_object_unref (next_file);
                g_object_ref (info);
        }

        files = g_list_reverse (files);

        for (GList *l = files; l != NULL; l = l->next) {
                GFile *file = l->data;

                char *relative_path = g_file_get_relative_path (base, file);
                char *test_path = g_strconcat (prefix, "/", relative_path, NULL);

                g_test_add_data_func_full (test_path, g_object_ref (file), test_func, g_object_unref);

                g_free (test_path);
                g_free (relative_path);
        }

        g_list_free_full (files, g_object_unref);
}

static void
valid_image (gconstpointer user_data)
{
        GFile *file = G_FILE (user_data);

        GError *error = NULL;
        char *buffer = NULL;
        gsize size = 0;

        g_test_message ("Loading image: '%s'", g_file_peek_path (file));

        g_file_load_contents (file, NULL, &buffer, &size, NULL, &error);
        g_assert_no_error (error);

        GdkPixbufLoader *loader = gdk_pixbuf_loader_new ();
        gdk_pixbuf_loader_write (loader, (const guchar *) buffer, size, &error);
        g_assert_no_error (error);

        gdk_pixbuf_loader_close (loader, &error);
        g_assert_no_error (error);

        g_object_unref (loader);
        g_free (buffer);
}

static void
invalid_image (gconstpointer user_data)
{
        GFile *file = G_FILE (user_data);

        GError *error = NULL;
        char *buffer = NULL;
        gsize size = 0;

        g_test_message ("Loading image: '%s'", g_file_peek_path (file));

        g_file_load_contents (file, NULL, &buffer, &size, NULL, &error);
        g_assert_no_error (error);

        GdkPixbufLoader *loader = gdk_pixbuf_loader_new ();
        gdk_pixbuf_loader_write (loader, (const guchar *) buffer, size, &error);
        g_assert_no_error (error);

        gdk_pixbuf_loader_close (loader, &error);
        g_assert_nonnull (error);
        g_assert_true (error->domain == GDK_PIXBUF_ERROR);

        g_error_free (error);
        g_object_unref (loader);
        g_free (buffer);
}

int
main (int argc,
      char *argv[])
{
        g_test_init (&argc, &argv, NULL);

        char *base_dir = base_dir = g_build_filename (g_test_get_dir (G_TEST_DIST), "test-images", NULL);
        GFile *base = g_file_new_for_path (base_dir);

        GFile *valid = g_file_get_child (base, "valid");
        add_test_for_all_images ("/pixbuf/valid", valid, valid_image);
        g_object_unref (valid);

        GFile *invalid = g_file_get_child (base, "invalid");
        add_test_for_all_images ("/pixbuf/invalid", invalid, invalid_image);
        g_object_unref (invalid);

        g_object_unref (base);
        g_free (base_dir);

        return g_test_run ();
}
